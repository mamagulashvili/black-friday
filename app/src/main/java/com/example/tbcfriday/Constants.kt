package com.example.tbcfriday

import java.util.regex.Pattern

object Constants {
    const val NAME_REGEX = "^\\p{L}+[\\p{L}\\p{Z}\\p{P}]{0,}"
    const val EXTRA_USER = "extra_user"

}