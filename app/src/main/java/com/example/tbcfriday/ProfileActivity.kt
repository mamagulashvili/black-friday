package com.example.tbcfriday

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.tbcfriday.Constants.EXTRA_USER
import com.example.tbcfriday.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    private val imageRequestCode = 111

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    private fun init() {
        backgroundAnimation()
        getUserInfo()
        binding.profileImage.setOnClickListener {
            Intent(Intent.ACTION_GET_CONTENT).also {
                it.type = "image/*"
                startActivityForResult(it, imageRequestCode)
            }
        }
    }

    private fun getUserInfo() {
        val user: User = intent?.extras?.getParcelable(EXTRA_USER)!!
        binding.apply {
            emailTV.text = user.email
            firstNameTV.text = user.firstName
            lastNameTV.text = user.lastName
            ageTV.text = user.age
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == imageRequestCode) {
            val uri = data?.data
            binding.profileImage.setImageURI(uri)
        }
    }

    private fun backgroundAnimation() {
        val drawableAnimation: AnimationDrawable =
            binding.profileContainer.background as AnimationDrawable
        drawableAnimation.apply {
            setEnterFadeDuration(1300)
            setExitFadeDuration(3000)
            start()
        }
    }
}