package com.example.tbcfriday

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Log.d
import android.util.Patterns
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.example.tbcfriday.Constants.EXTRA_USER
import com.example.tbcfriday.Constants.NAME_REGEX
import com.example.tbcfriday.databinding.ActivityMainBinding
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern


@RequiresApi(Build.VERSION_CODES.N)
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var activeUser = 0
    private var deletedUser = 0
    private val userContainer = mutableMapOf<Int, User>()
    private var userIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    private fun init() {
        validUserInputs()
        binding.apply {
            addUserButton.setOnClickListener {
                addUser()
            }
            removeUserButton.setOnClickListener {
                deleteUser()
            }
            updateUserButton.setOnClickListener {
                updateUser()
            }
            showCurrentUserButton.setOnClickListener {
                showCurrentUserProfile()
            }
        }

    }

    private fun showCurrentUserProfile() {
        val email = binding.emailET.text.toString().trimStart()
        val firstName = binding.firstNameET.text.toString().trimStart()
        val lastName = binding.lastNameET.text.toString().trimStart()
        val age = binding.ageET.text.toString().trimStart()
        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {
            val user = User(email, firstName, lastName, age)
            if (userContainer.containsValue(user)) {
                val intent = Intent(this, ProfileActivity::class.java).apply {
                    putExtra(EXTRA_USER, user)
                }
                startActivity(intent)
            } else {
                Snackbar.make(binding.mainContainer, "Please add this user", Snackbar.LENGTH_SHORT)
                    .apply {
                        setTextColor(Color.RED)
                    }.show()
            }

        } else {
            emptyInputErrorText()
        }
    }

    private fun addUser() {
        val email = binding.emailET.text.toString().trimStart()
        val firstName = binding.firstNameET.text.toString().trimStart()
        val lastName = binding.lastNameET.text.toString().trimStart()
        val age = binding.ageET.text.toString().trimStart()
        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {
            val user = User(email, firstName, lastName, age)
            if (userContainer.containsValue(user)) {
                Snackbar.make(binding.mainContainer, "User already exists", Snackbar.LENGTH_SHORT)
                    .apply {
                        setTextColor(Color.RED)
                    }.show()

            } else {
                userContainer[userIndex] = user
                userIndex++
                activeUser++
                binding.numberOfUser.text = activeUser.toString()
                d("TAG", "$userContainer")
                Snackbar.make(
                    binding.mainContainer,
                    "User added successfully",
                    Snackbar.LENGTH_SHORT
                )
                    .apply {
                        setTextColor(Color.GREEN)
                    }.show()
            }
        } else {
            emptyInputErrorText()

        }
    }

    private fun deleteUser() {
        val email = binding.emailET.text.toString().trimStart()
        val firstName = binding.firstNameET.text.toString().trimStart()
        val lastName = binding.lastNameET.text.toString().trimStart()
        val age = binding.ageET.text.toString().trimStart()

        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {
            val currentUser = User(email, firstName, lastName, age)
            if (userContainer.containsValue(currentUser)) {
                val keys =  userContainer.filterValues { it == currentUser }.keys.toMutableList()
                d("TAG", "$keys")
                userContainer.remove(keys[0])
                deletedUser++
                activeUser--
                binding.apply {
                    numberOfUser.text = activeUser.toString()
                    numberOfDeletedUser.text = deletedUser.toString()
                }
                Snackbar.make(
                    binding.mainContainer,
                    "User deleted successfully",
                    Snackbar.LENGTH_SHORT
                )
                    .apply {
                        setTextColor(Color.GREEN)
                    }.show()
                d("TAG", "$userContainer")
            } else {
                Snackbar.make(binding.mainContainer, "User does not exits", Snackbar.LENGTH_SHORT)
                    .apply {
                        setTextColor(Color.RED)
                    }.show()
            }
        } else {
            emptyInputErrorText()
        }
    }

    private fun updateUser() {
        val email = binding.emailET.text.toString().trimStart()
        val firstName = binding.firstNameET.text.toString().trimStart()
        val lastName = binding.lastNameET.text.toString().trimStart()
        val age = binding.ageET.text.toString().trimStart()
        if (email.isNotEmpty() && firstName.isNotEmpty() && lastName.isNotEmpty() && age.isNotEmpty()) {

            val user = User(email, firstName, lastName, age)
            val keys = userContainer.filterValues { it.email == user.email }.keys.toMutableList()
            d("TAG", "$keys")
            userContainer.replace(keys[0],user)
            Snackbar.make(
                binding.mainContainer,
                "User updated successfully",
                Snackbar.LENGTH_SHORT
            )
                .apply {
                    setTextColor(Color.GREEN)
                }.show()
            d("TAG", "$userContainer")
        } else {
            emptyInputErrorText()
        }
    }

    private fun validUserInputs() {
        binding.apply {
            emailET.doOnTextChanged { text, _, _, _ ->
                if (!text?.let { isValidEmail(it) }!!) {
                    binding.emailContainer.error = "Incorrect Email "
                } else {
                    binding.emailContainer.error = null
                }
            }
            firstNameET.doOnTextChanged { text, _, _, _ ->
                if (Pattern.matches(NAME_REGEX, text!!)) {
                    binding.firstNameContainer.error = null
                } else {
                    binding.firstNameContainer.error = "InCorrect Name"

                }
            }
            lastNameET.doOnTextChanged { text, _, _, _ ->
                if (Pattern.matches(NAME_REGEX, text!!)) {
                    binding.lastNameContainer.error = null
                } else {
                    binding.lastNameContainer.error = "InCorrect Last Name"
                }
            }
        }
    }

    private fun emptyInputErrorText() {
        Snackbar.make(binding.mainContainer, "Please fill all field", Snackbar.LENGTH_SHORT)
            .apply {
                setTextColor(Color.RED)
            }.show()
    }

    private fun isValidEmail(email: CharSequence): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }
}